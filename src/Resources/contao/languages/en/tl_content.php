<?php

/**
 * 361GRAD Element Factsteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = "DSE-Elements";
$GLOBALS['TL_LANG']['CTE']['dse_factsteaser'] = ["Facts and figures Teaser", "Facts and figures Teaser"];

$GLOBALS['TL_LANG']['tl_content']['dse_figure'] =
    ['Figures headline', 'Here you can add a figures headline.'];
