<?php

/**
 * 361GRAD Element Factsteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_factsteaser'] =
    'Dse\\ElementsBundle\\ElementFactsteaser\\Element\\ContentDseFactsteaser';

// Cascading Style Sheets
$GLOBALS['TL_CSS']['dse_factsteaser'] = 'bundles/dseelementfactsteaser/css/fe_ce_dse_factsteaser.css';
