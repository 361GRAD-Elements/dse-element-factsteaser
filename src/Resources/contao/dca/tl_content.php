<?php

/**
 * 361GRAD Element Factsteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_factsteaser'] =
    '{type_legend},type,dse_figure,text;' .
    '{invisible_legend:hide},invisible,start,stop,dse_data';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_figure'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_figure'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'clr'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
