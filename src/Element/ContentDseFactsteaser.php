<?php

/**
 * 361GRAD Element Factsteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementFactsteaser\Element;

use Contao\ContentElement;
use Contao\StringUtil;
use Patchwork\Utf8;

/**
 * Class ContentDseFactsteaser
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentDseFactsteaser extends ContentElement
{
    /**
     * Template
     *
     * @var string
     */
    protected $strTemplate = 'ce_dse_factsteaser';


    /**
     * Display a wildcard in the back end
     *
     * @return string
     */
    public function generate()
    {
        return parent::generate();
    }


    /**
     * Generate the module
     */
    protected function compile()
    {
        $arrSubheadline              = StringUtil::deserialize($this->dse_subheadline);
        $this->Template->subheadline = is_array($arrSubheadline) ? $arrSubheadline['value'] : $arrSubheadline;
        $this->Template->shl         = is_array($arrSubheadline) ? $arrSubheadline['unit'] : 'h2';
    }
}
